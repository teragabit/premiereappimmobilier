import React from 'react'

const ProgressBar = () => {
    return (
        <div>
            <div className="percentage">
                <div className="progressPercentage">Question: 1/10</div>
                <div className="progressPercentage">Progression: 10%</div>
            </div>
            <div className="progress">
                <div className="progress-bar progress-bar-striped progress-bar-animated" style={{width: '10%'}}></div>
            </div>
        </div>
    )
}

export default ProgressBar
