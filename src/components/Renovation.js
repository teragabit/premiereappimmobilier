import React from 'react';
import Menu from './Menu'
import Footer from './Footer'

const Renovation = () => {
    return (
        <div>
            <Menu />
            <div className="menuDeroulant">
                <ul>
                    <li> <a href="#">Plombier</a></li>
                    <li> <a href="#"> Electricien</a></li>
                    <li> <a href="#">Menuisier</a></li>
                    <li> <a href="#">maçon</a></li>
                    <li> <a href="#">Aluminium</a></li>
                </ul>
            </div>
            <Footer />
        </div>
 
    )
}
export default Renovation;