import React from 'react';
import Menu from './Menu'
import Footer from './Footer'

const Menage = () => {
    return (
        <div>
            <Menu />
            <div className="menuDeroulant">
                <ul>
                    <li> <a href="#">la ménagère</a></li>
                    <li> <a href="#">service d' entretien</a></li>
                    <li> <a href="#">Autres</a></li>
                    <li> <a href="#">Gardinier</a></li>
                </ul>
            </div>
            <Footer />
        </div>
    )
}
export default Menage;