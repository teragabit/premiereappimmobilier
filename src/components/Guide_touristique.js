import React from 'react';
import Menu from './Menu'
import Footer from './Footer'

const Guide_touristique = () => {
    return (
        <div>
            <Menu />
            <div className="menuDeroulant">
                <ul>
                    <li> <a href="#">Cameroun</a></li>
                    <li> <a href="#">centre</a></li>
                    <li> <a href="#">Litoral</a></li>
                    <li> <a href="#">Ouest</a></li>
                    <li> <a href="#">Est</a></li>
                    <li> <a href="#">Nord-Ouest</a></li>
                    <li> <a href="#">Sud-ouest</a></li>
                    <li> <a href="#">Nord</a></li>
                    <li> <a href="#">Exprem-Nord</a></li>
                    <li> <a href="#">Adamaoua</a></li>
                    <li> <a href="#">Sud-ouest</a></li>
                </ul>
            </div>
            <Footer />
        </div>
    )
}
export default Guide_touristique;