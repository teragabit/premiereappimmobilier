import React, { Component } from 'react'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css'
import Levels from './Levels'
import ProgressBar from './ProgressBar'
import { MonQuiz } from './MonQuiz';
import Menu from './Menu'
import MenuConnecter from './MenuConnecter'
import Footer from './Footer'


toast.configure();

class Quiz extends Component {

    state = {
        levelNames: ["debutant", "confirme", "expert"],
        QuizLevel: 0,
        maxQuestions: 10,
        stordQuestions: [],
        question: null,
        options: [],
        idQuestion: 0,
        btnDisabled: true,
        userAnswer: null,
        score: 0,
        showWelcomeMsg: false
    }
    storeDataRef = React.createRef();

    loadQuestions = quizz => {
       const fetchedArrayQuiz = MonQuiz[0].quizz[quizz];
       if(fetchedArrayQuiz.length >= this.state.maxQuestions){

           this.storeDataRef.current = fetchedArrayQuiz;

            const newArray = fetchedArrayQuiz.map( ({answer, ...keepRest}) => keepRest);

            this.setState({
                stordQuestions: newArray
            })

       }else{
           console.log("nombre de questions insuffisant");
       }
    }
    
    showWelcomeMessage = pseudo => {
        if(!this.state.showWelcomeMsg){

            this.setState({
                showWelcomeMsg: true
            })
            toast.warn(`Bienvenue ${this.props.userData.pseudo}, et bonne chance!`, {
                position: "top-right",
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            })
        }
    }

    componentDidMount() {
        this.loadQuestions(this.state.levelNames[this.state.QuizLevel])
    }

    nextQuestion = () => {
        if(this.state.idQuestion === this.state.maxQuestions){
            //end
        }else{
            this.setState(prevState => ({
                idQuestion: prevState.idQuestion + 1
            }))
        }

        const goodAnswer = this.storeDataRef.current[this.state.idQuestion].answer;
        if(this.state.userAnswer === goodAnswer){
            this.setState(prevState => ({
                score: prevState.score + 1
            }))
            toast.success(`Bravo!  +1`, {
                position: "top-right",
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                bodyClassName: "toastify-color"

                });
        }else{
            toast.error(`Uppss!  0`, {
                position: "top-right",
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                bodyClassName: "toastify-color"

                });
        }
    }

    componentDidUpdate(prevProps, prevState){
        if(this.state.stordQuestions !== prevState.stordQuestions){
            
            this.setState({
                question: this.state.stordQuestions[this.state.idQuestion].question,
                options: this.state.stordQuestions[this.state.idQuestion].options
            })
        }

        if(this.state.idQuestion !== prevState.idQuestion){
            this.setState({
                question: this.state.stordQuestions[this.state.idQuestion].question,
                options: this.state.stordQuestions[this.state.idQuestion].options,
                userAnswer: null,
                btnDisabled: true
            })
        }
        if(this.props.userData.pseudo){
            this.showWelcomeMessage(this.props.userData.pseudo);
        }

    }
    submitAnswer = selectedAnswer => {
        this.setState({
            userAnswer: selectedAnswer,
            btnDisabled: false
        })

    }
    
    render() {

        const displayOptions = this.state.options.map((option, index) => {
            return(
                <p key={index} 
                 className={`answerOptions ${this.state.userAnswer === option ? "selected" : null}`}
                 onClick={() => this.submitAnswer(option)}
                >
                    {option}
                    </p>
            )
        })

        return (
            <div>
                <Menu />
                <div className="Quiz">
                    <Levels />
                    <ProgressBar />
                    <h2>{this.state.question}</h2>

                    { displayOptions }

                    <div className="QuizButton">
                        <button 
                            disabled={this.state.btnDisabled} 
                            className="btn btn-primary btn-sm"
                            onClick={this.nextQuestion}
                        >
                            Suivant
                        </button>
                    </div>
                </div>
                <Footer />
            </div>
        )
    }
}

export default Quiz
