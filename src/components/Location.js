import React from 'react';
import Menu from './Menu'
import Footer from './Footer'

const Location = () => {
    return (
        <div>
            <Menu />
            <div className="menuDeroulant">
                <ul>
                    <li> <a href="#">Chambre</a></li>
                    <li> <a href="#">Studio</a></li>
                    <li> <a href="#">Appartement</a></li>
                    <li> <a href="#">Bureau</a></li>
                    <li> <a href="#">Salle de fête</a></li>
                    <li> <a href="#">Hotel</a></li>
                    <li> <a href="#">Meublé</a></li>
                </ul>
            </div>
            <Footer />
        </div>
    )
}
export default Location;