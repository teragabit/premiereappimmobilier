import React, { Component }  from 'react';

class Poster extends Component{


    render(){

        
        return (
           <div className="poster">
                <form method="post" encType="multipart/form-data" >
                <p className="textPost">Poster maintenant votre immobilier</p>
                <div className="lienPost">
                    <div id="depose">deposez vos images ou cliquez pour les choisir</div>
                    <input type="file" name="monFichier" accept="image/*" required multiple />
                    </div>
                </form>
            </div>
        );
    }
}

export default Poster;