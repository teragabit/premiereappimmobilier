import React from 'react';
import Menu from './Menu'
import Footer from './Footer'

const Demenagement = () => {
    return (
        <div>
            <Menu />
            <div className="menuDeroulant">
                <ul>
                    <li> <a href="#">Chargeur</a></li>
                    <li> <a href="#">Moto transporteur</a></li>
                    <li> <a href="#">Taxi transporteur</a></li>
                    <li> <a href="#">cargo transporteur</a></li>
                    <li> <a href="#">gros transporteur</a></li>
                    <li> <a href="#">Autres</a></li>
                </ul>
            </div>
            <Footer />
        </div>
    )
}
export default Demenagement;