import React from 'react';
import Menu from './Menu'
import Footer from './Footer'

const Vente = () => {
    return (
        <div>
            <Menu />
            <div className="menuDeroulant">
                <ul>
                    <li> <a href="#">Maison</a></li>
                    <li> <a href="#">Terrain</a></li>
                    <li> <a href="#">Meuble</a></li>
                    <li> <a href="#">Appareil numerique</a></li>
                    <li> <a href="#">Appareil elector-menager</a></li>
                    <li> <a href="#">Telephone</a></li>
                    <li> <a href="#">Autres</a></li>
                </ul>
            </div>
            <Footer />
        </div>
    )
}
export default Vente;